import React, { Component }  from 'react'
import * as THREE from "three";
import ReactDOM from 'react-dom';
import Container3d from 'react-container-3d';

let container;

export class ModelViewer extends Component {
  constructor(props) {
    super(props)
    let presets = {}
    this.state = presets
  }

  Setup = (scene, camera, renderer) =>{

    // Add ground for game 

    //  Add object over the ground
    
    // Creating Cube for ground
    var cube_geometry = new THREE.BoxGeometry( 5.0, 5.0, 5.0  );
    var color = new THREE.Color('red');
    var cube_material = new THREE.MeshLambertMaterial ( { color: color } );
    var cube = new THREE.Mesh( cube_geometry, cube_material );
    cube.castShadow = true;
    cube.position.set( 0.0, 2.0, 0.0 );
    scene.add(cube)
    // Add group into scene

  }

  getDomContainer = () =>{
    return ReactDOM.findDOMNode(container);
  }

  render() {
    return (
      <div className="content-wrapper container-3d container" style={{width: '100%', height: '100%', position:'absolute', marginLeft:'0'}}>
        <Container3d
          className="canvas-3d"
          percentageWidth={'100%'}
          fitScreen
          ref={c => (container = c)}
          marginBottom={30}
          addLight={true}
          addControls={true}
          addGrid={false}
          antialias={false}
          setup={this.Setup}
        />
      </div>
    )
  }
}